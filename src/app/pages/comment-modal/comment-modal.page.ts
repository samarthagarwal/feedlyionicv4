import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavParams, ModalController } from '@ionic/angular';
import * as moment from 'moment';
import * as firebase from 'firebase';

@Component({
  selector: 'app-comment-modal',
  templateUrl: './comment-modal.page.html',
  styleUrls: ['./comment-modal.page.scss'],
})
export class CommentModalPage {

  comments: any[] = [];
  post: any = {};
  modalCtrl: ModalController

  constructor(private navParams: NavParams, public router: Router) {

    this.post = this.navParams.get("post");
    this.modalCtrl = this.navParams.get("modal");


    firebase.firestore().collection("comments")
    .where("post", "==", this.post.id)
    .orderBy("created", "asc")
    .get().then((docs) => {
      docs.forEach((doc) => {
        this.comments.push(doc);
      })
      console.log(this.comments)
    })
  }

  ago(param){
    let d = moment(param).diff(moment());
    return moment.duration(d).humanize();
  }

  close(){
    this.modalCtrl.dismiss();
  }

}
