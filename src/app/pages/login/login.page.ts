import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from "@angular/router"
import { ToastController, NavController } from '@ionic/angular';
import * as firebase from 'firebase';

declare var navigator;

@Component({
  selector: 'app-page-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginPage {

  email: string = "";
  password: string = "";

  constructor(private toastCtrl: ToastController, private navCtrl: NavController) {
    firebase.auth().onAuthStateChanged(async (user) => {
      //console.log(user)
      if (user) {
        let toast = await this.toastCtrl.create({
          message: "Logging you in as " + user.displayName,
          duration: 5000
        })

        toast.present();
        this.navCtrl.goRoot('/feeds')
      }
    })

  }

  gotoSignupPage() {
    this.navCtrl.goForward('/signup')
  }

  login() {

    firebase.auth().signInWithEmailAndPassword(this.email, this.password).then((user) => {
      console.log(user);
      this.navCtrl.goRoot('/feeds')
    }).catch(async (err) => {
      console.log(err)
      let toast = await this.toastCtrl.create({
        message: err.message,
        duration: 5000
      })

      toast.present();
    })

  }

}
