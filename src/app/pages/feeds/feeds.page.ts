import { Component, OnInit, ViewEncapsulation, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, ActionSheetController, ToastController, AlertController, ModalController, LoadingController, PopoverController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';
import * as firebase from 'firebase';
//import 'firebase/firestore';

import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Firebase } from '@ionic-native/firebase/ngx';
import { CommentModalPage } from '../comment-modal/comment-modal.page';

@Component({
  selector: 'app-feeds',
  templateUrl: './feeds.page.html',
  styleUrls: ['./feeds.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FeedsPage {

  stopListening: any;
  text: string = "";
  posts: any[] = [];
  cursor: any;
  pageSize: number = 10;
  infiniteScrollEvent: any;
  user: any = {};
  image: string;

  constructor(private navCtrl: NavController, private actionsheetCtrl: ActionSheetController, private alertCtrl: AlertController, private toastCtrl: ToastController, private modalCtrl: ModalController, private http: HttpClient, private loadingCtrl: LoadingController, private popoverCtrl: PopoverController, private ngZone: NgZone, private camera: Camera, private firebaseCordova: Firebase) {

    this.posts = [];
    this.getPosts(null);

    this.user = firebase.auth().currentUser;

    firebase.auth().onAuthStateChanged(async (user) => {
      console.log(user)
      if (user) {
        this.user = user
      }
    })

    this.firebaseCordova.getToken().then((token) => {
      this.updateToken(firebase.auth().currentUser.uid, token);
    }).catch(error => console.log('Error getting token'+  JSON.stringify(error)));

    this.firebaseCordova.onTokenRefresh().subscribe((token) => {
      this.updateToken(firebase.auth().currentUser.uid, token);
    }, error => {
      console.log('Error getting token'+  JSON.stringify(error))
    });


  }

  updateToken(uid: string, token: string) {

    //The following code update the token value in the user/<uid> document. If the document does not exist, the document is created.

    firebase.firestore().collection("users").doc(uid).set({
      token: token,
      tokenUpdated: firebase.firestore.FieldValue.serverTimestamp()
    }, { merge: true }).then(() => {
      console.log("Token saved to cloud firestore");
    }).catch((err) => {
      console.log(err)
    });
  }

  ago(param) {
    let d = moment(param).diff(moment());
    return moment.duration(d).humanize();
  }

  getPosts(event) {

    console.log(event)

    let query;

    if (this.cursor) {
      query = firebase.firestore().collection("posts").orderBy("created", "desc").startAfter(this.cursor).limit(this.pageSize);
    } else {
      query = firebase.firestore().collection("posts").orderBy("created", "desc").limit(this.pageSize);
    }

    this.stopListening = query.onSnapshot((snapshot) => {
      console.log(snapshot)
      snapshot.docChanges().forEach((change) => {
        console.log(change)
        if (change.type == "added") {
          //Do Nothing
        }
        if (change.type == "modified") {
          //console.log("Modified: ", change.doc.data);
          for (let i = 0; i < this.posts.length; i++) {
            if (this.posts[i].id == change.doc.id) {
              this.posts[i] = change.doc;
              console.log("Doc Updated in Array")
            }
          }
        }
        if (change.type == "removed") {
          //Do nothing
        }
      });
    });

    query.get().then(async (documentSnapshots) => {
      documentSnapshots.forEach((doc) => {
        this.ngZone.run(() => {
          this.posts.push(doc);
        })
      })

      console.log(documentSnapshots.size);

      //If all posts are loaded and there are no more to load
      if (documentSnapshots.size < this.pageSize && event && typeof event.enable == "function") {
        this.infiniteScrollEvent = event;
        event.complete();
        event.enable(false);
        let toast = await this.toastCtrl.create({
          message: "All posts are loaded.",
          duration: 3000
        })

        toast.present();
      } else { //If there are more posts to be loaded
        this.cursor = documentSnapshots.docs[documentSnapshots.docs.length - 1];
        if (event) {
          console.log(event)
          event.complete();
        }
      }
      console.log(this.cursor)
      console.log(this.posts)
    })
  }

  loadMorePosts(event) {
    this.getPosts(event);
  }

  refresh(event) {
    this.posts = [];
    this.cursor = undefined;
    this.stopListening();
    if (this.infiniteScrollEvent) this.infiniteScrollEvent.enable(true);
    this.getPosts(event);
  }

  post() {
    firebase.firestore().collection("posts").add({
      text: this.text,
      created: firebase.firestore.FieldValue.serverTimestamp(),
      owner: firebase.auth().currentUser.uid,
      owner_name: firebase.auth().currentUser.displayName
    }).then(async (doc) => {
      if (this.image)
        await this.upload(doc.id);
      
      let toast = await this.toastCtrl.create({
        message: "Posted Successfully!",
        duration: 3000
      })
      toast.present();
      this.refresh(null)
      this.text = "";
      this.image = undefined;
    })
  }

  async like(post) {

    console.log(post.data())

    let body = {
      "postId": post.id,
      "userId": firebase.auth().currentUser.uid,
      "action": post.data().likes && post.data().likes[firebase.auth().currentUser.uid] == true ? "unlike" : "like"
    }

    console.log(body)

    let toast = await this.toastCtrl.create({
      message: "Updating like... Please wait."
    });

    toast.present();
    
    this.http.post(
      "https://us-central1-feedly-a36e4.cloudfunctions.net/updateLikesCount",
      body,
      { responseType: 'text' }
    ).subscribe((res) => {
      console.log("Like Updated", res);
      toast.message = "Like updated.";
      setTimeout(() => {
        toast.dismiss();
      }, 3000);
    }, (err) => {
      console.log("Err", err)
      toast.message = "An error occurred. Please try again.";
      setTimeout(() => {
        toast.dismiss();
      }, 3000);
    });
  }

  comment(post) {
    this.actionsheetCtrl.create({
      buttons: [{
        text: "View All Comments",
        handler: async () => {
          let modal = await this.modalCtrl.create({
            component: CommentModalPage,
            componentProps: {
              post: post,
              modal: this.modalCtrl
            }
          });
          modal.present();
        }
      }, {
        text: "New Comment",
        handler: () => {
          this.alertCtrl.create({
            header: "New Comment",
            message: "Type your comment",
            inputs: [{
              name: "comment",
              type: "text"
            }],
            buttons: [{
              text: "Cancel"
            }, {
              text: "Post",
              handler: (data) => {
                if (data.comment) {
                  console.log(data.comment)
                  firebase.firestore().collection("comments").add({
                    text: data.comment,
                    post: post.id,
                    owner: firebase.auth().currentUser.uid,
                    owner_name: firebase.auth().currentUser.displayName,
                    created: firebase.firestore.FieldValue.serverTimestamp()
                  }).then(async (done) => {
                    let toast = await this.toastCtrl.create({
                      message: "Comment Posted Successfully!",
                      duration: 3000
                    });
                    toast.present();
                  }).catch(async (err) => {
                    let toast = await this.toastCtrl.create({
                      message: err.message,
                      duration: 3000
                    });
                    toast.present();
                  })
                }
              }
            }]
          }).then((alert) => {
            alert.present();
          })
        }
      }]
    }).then((alert) => {
      alert.present();
    })
  }

  getLikeColor(likes) {
    if (likes && likes[this.user.uid] == true) {
      return 'danger';
    } else {
      return 'medium';
    }
  }

  addPhoto() {
    this.launchCamera();
  }

  launchCamera() {
    const options: CameraOptions = {
      quality: 100,
      sourceType: this.camera.PictureSourceType.CAMERA,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.PNG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      targetHeight: 500,
      targetWidth: 500,
      allowEdit: true
    }

    let base64Image = ""

    this.camera.getPicture(options).then((imageData: String) => {

      base64Image = 'data:image/png;base64,' + imageData;
      console.log(base64Image)

      this.image = base64Image;

    }, (err) => {
      console.log(err)
    });
  }

  async upload(postId) {
    let blob;

    blob = this.b64toBlob(this.image.split(',')[1], "image/png");

    let storage = firebase.storage();
    let loading = await this.loadingCtrl.create({
      content: "Uploading Photo..."
    });
    loading.present();

    let ref = storage.ref('postImages/' + postId).put(blob)

    ref.on("state_changed", (taskSnapshot: any) => {
      if (taskSnapshot.state == firebase.storage.TaskState.RUNNING)
        loading.content = ((taskSnapshot.bytesTransferred * 100 / taskSnapshot.totalBytes).toFixed(0) + "% Completed");
    }, (error) => {
      console.log(error)
      loading.dismiss();
    }, async () => {
      loading.dismiss();

      let db = firebase.firestore();
      await db.collection("posts").doc(postId).update({
        image: ref.snapshot.downloadURL
      });

    })
  }

  b64toBlob(b64Data, contentType, sliceSize?) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;
    console.log(b64Data)
    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      var slice = byteCharacters.slice(offset, offset + sliceSize);

      var byteNumbers = new Array(slice.length);
      for (var i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      var byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
  }

  async logout() {

    let toast = await this.toastCtrl.create({
      message: "You have been logged out successfully.",
      duration: 3000
    });

    this.firebaseCordova.unregister().then(() => {
      firebase.auth().signOut().then(() => {
        
        toast.present()
        this.navCtrl.goRoot('/login');

      })
    }).catch((err) => {
      console.log(err);
      firebase.auth().signOut().then(() => {

        toast.present()
        this.navCtrl.goRoot('/login');

      })
    });
  }

}
