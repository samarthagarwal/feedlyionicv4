import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { FeedsPage } from './feeds.page';
import { CommentModalPage } from '../comment-modal/comment-modal.page';

const routes: Routes = [
  {
    path: '',
    component: FeedsPage
  }
];

// To make a modal work, you can either add it to the entry components array or just add it to the routes above.

@NgModule({
  entryComponents: [CommentModalPage], //THIS IS VERY IMPORTANT
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FeedsPage, CommentModalPage]
})
export class FeedsPageModule {}
