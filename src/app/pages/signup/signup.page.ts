import { Component, ViewEncapsulation } from '@angular/core';
import { AlertController, ToastController, NavController } from '@ionic/angular';
import * as firebase from 'firebase';
import 'firebase/firestore';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SignupPage {

  email: string = "";
  name: string = "";
  password: string = "";

  constructor( private alertCtrl: AlertController, private toastCtrl: ToastController, private navCtrl: NavController) {
    
  }

  goBack(){
    this.navCtrl.goBack('/login')
  }

  signup(){

    firebase.auth().createUserWithEmailAndPassword(this.email, this.password).then((res) => {
      console.log(res)
      res.user.updateProfile({displayName: this.name, photoURL: ""}).then(async () => {
        let alert = await this.alertCtrl.create({
          header: "Account Created",
          message: "Your account has been created successfully. Please login to proceed further.",
          buttons: [{
            text: "OK",
            handler: () => {
              this.navCtrl.goRoot('/feeds')
            }
          }]
        });
        
        alert.present();
      });
    }).catch(async (err) => {
      let toast = await this.toastCtrl.create({
        message: err.message,
        duration: 3000
      });

      toast.present();
    })

  }

}
