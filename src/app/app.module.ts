import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, RouteReuseStrategy, Routes } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { FormsModule } from '@angular/forms';

import { Camera } from '@ionic-native/camera/ngx';
import { Firebase } from '@ionic-native/firebase/ngx';

import * as firebase from 'firebase';
import 'firebase/firestore';
import { HttpClientModule } from '@angular/common/http';

var config = {
  apiKey: "AIzaSyDNsGvlekmFCU6-qb6zway7wypZIR8Fym4",
  authDomain: "feedly-a36e4.firebaseapp.com",
  databaseURL: "https://feedly-a36e4.firebaseio.com",
  projectId: "feedly-a36e4",
  storageBucket: "feedly-a36e4.appspot.com",
  messagingSenderId: "179123632235"
};
firebase.initializeApp(config);
firebase.firestore().settings({
  timestampsInSnapshots: true
});

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, FormsModule, HttpClientModule],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    Firebase,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
